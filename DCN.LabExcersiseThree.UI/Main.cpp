
//MadLibs (Lab Excersise 3)
// Aaliyah, Devon, Nathan, Sydney, Zachary

#include <iostream>
#include <conio.h>
#include <fstream>
#include <string>

using namespace std;

void MadLib(string *madLibInput, ostream& os = cout) 
{
	os << " Once Upon a time, a " << madLibInput[0] << " team known as The " << madLibInput[1]
		<< " made famous by their star player, " << madLibInput[2] << " had a rather " << madLibInput[3]
		<< " ordeal going on." << "\n" << " The baseball team found themselves under attack from the 32 "
		<< madLibInput[4] << ". This was a very serious situation for the team, since \n" <<
		"they were in the middle of eating their signiture" << madLibInput[5] << " dinner! And out of nowhere, arrived the "
		<< madLibInput[6] << " mastermind known as " << madLibInput[7] << "\n"
		<< " which, instead of attacking, asked the team a question. As such, they all met at " << madLibInput[8]
		<< " and decied to " << madLibInput[9] << " a serving of " << madLibInput[5]; //Reuse of the food noun.
}


int main()
{
	//sets the array of numLimit to a limit of 10.
	const int numLimit = 10;
	char save = ' ';

	//Establishes a ofstream ofs for madlib.txt
	ofstream ofs("madlib.txt");


	//Creates a string based off the array of numLimit.
	string madLibInput[numLimit];
	madLibInput[0] = "Enter An Adjective: "; //Adjective
	madLibInput[1] = "Enter A Baseball Team: "; //Baseball Team
	madLibInput[2] = "Enter A Person: "; //Person
	madLibInput[3] = "Enter An Adjective: "; //Adjective
	madLibInput[4] = "Enter A Plural Noun: "; //Plural Noun
	madLibInput[5] = "Enter A Food Item: "; //Food
	madLibInput[6] = "Enter A Adjective: "; //Adjective
	madLibInput[7] = "Enter A Person: "; //Person
	madLibInput[8] = "Enter A Place: "; //Place
	madLibInput[9] = "Enter A Verb: "; //Verb

	//for loop to input a string value for each madLibInput
	for (int i = 0; i < numLimit; i++)
	{
		//prints out the madLibInput array number
		cout << madLibInput[i];
		//prompts user to input a string based upon the current question
		cin >> madLibInput[i];
		//Prints out a line break
		cout << "\n";
	}

	MadLib(madLibInput);
	cout << "\n"; //placeholder(?)
	cout << "Would You Like To Save This Story? (enter y/n)";
	cin >> save;


	if (save == 'y' || save == 'Y')
	{
		MadLib(madLibInput, ofs);
		cout << "Madlib saved successfully!";
	}

	cout << "Press any button to continue";

	(void)_getch();
	return 0;
}
